#! /usr/bin/env python

import sys
import os
import glob
from os import path
import tkinter as Tk
import numpy as np

sys.path.append(path.join(os.getcwd(), "lib"))
from lib.GUI import ImagPanel  # noqa
from lib import funcs  # noqa
from lib import se  # noqa
from lib import alipylocal as alipy  # noqa


class MainApplication(Tk.Frame):
    def __init__(self, *args, **kwargs):
        # Set up path to data
        if os.name == "nt":
            list_of_dirs = filter(path.isdir, glob.glob(path.join("C:\\", "TelescopeData", "*")))
            self.path_to_data = max(list_of_dirs, key=path.getctime)
        else:
            self.path_to_data = "./tmp"

        self.reference_ascii_cat = None
        self.reference_se_cat = None
        # set up some lists
        self.fwhm_list = []
        self.peak_flux_list = []
        self.processed_files = []
        self.focus_values = []

        # GUI stuff
        self.root = Tk.Tk()
        self.root.title("Focus Helper")
        self.root.protocol('WM_DELETE_WINDOW', self.root.destroy)
        self.imagPanel = ImagPanel(self)
        self.root.after(0, self.look_for_focus_files)
        self.root.mainloop()

    def look_for_focus_files(self):
        """
        Function checks the working directory for a focus* files. If there are
        such files, it initiates computation
        """
        focus_files = glob.glob(path.join(self.path_to_data, "focus*"))
        if len(focus_files) == 0:
            # No focus files found in the directory. Wait two seconds and try again
            print("No focus files")
            self.root.after(200, self.look_for_focus_files)  # FIXME 2000
            return

        # Let's find the first file and use it as a reference
        first_focus_file = min(focus_files, key=path.getctime)
        self.processed_files.append(first_focus_file)
        se.call_SE(first_focus_file, "reference.cat")
        self.reference_se_cat = se.SExCatalogue("reference.cat")
        # Filter out FWHM outliers
        self.reference_se_cat.filter("FWHM_IMAGE", lambda x: x > 0.0)
        self.reference_se_cat.sigma_rejection("FWHM_IMAGE", 3)
        num_of_ref_stars = max(5, int(self.reference_se_cat.numOfObjects / 5))
        print(num_of_ref_stars)
        self.reference_stars = self.reference_se_cat.get_top_n("FLUX_AUTO", num_of_ref_stars)

        self.current_focus = funcs.get_focus_value(first_focus_file)
        # Now let's call a cycle function that performs seeing computation
        self.scores = [[1.0]]
        self.focus_values = [str(self.current_focus)]
        self.root.after(0, self.cycle)

    def cycle(self):
        """
        Function finds newest focus* file in the directory and performs the
        seeing computation for them
        """
        # Look for new files
        print(self.scores)
        all_files = glob.glob(path.join(self.path_to_data, "focus*"))
        newest_file = max(all_files, key=path.getctime)
        if newest_file in self.processed_files:
            # This file was analysed already
            self.root.after(1000, self.cycle)
            print("No new files")
            return
        new_focus_value = funcs.get_focus_value(newest_file)
        if self.current_focus != new_focus_value:
            self.current_focus = new_focus_value
            self.scores.append([])
            self.focus_values.append(str(self.current_focus))

        # We have found a new focus file. Let's perform the analysis
        self.processed_files.append(newest_file)

        # Call SExtractor to build a catalogue
        se.call_SE(newest_file, "catalogue.cat")
        se_cat = se.SExCatalogue("catalogue.cat")
        # Filter out FWHM outliers
        se_cat.filter("FWHM_IMAGE", lambda x: x > 0.0)
        se_cat.sigma_rejection("FWHM_IMAGE", 3)

        # Find shift between the reference catalogue and the current one
        ident = alipy.ident.run(self.reference_se_cat, se_cat)
        transform = ident.trans.inverse()

        relative_fluxes = []
        relative_fwhms = []
        relative_flux_radiuses = []
        # Calculate seeing score
        for star in self.reference_stars:
            # transform coorinates of the reference star
            star_x_trans, star_y_trans = transform.apply(star["X_IMAGE"], star["Y_IMAGE"])
            nearest_new_star = se_cat.find_nearest(star_x_trans, star_y_trans)
            if nearest_new_star is None:
                continue
            else:
                relative_fluxes.append(nearest_new_star["FLUX_AUTO"]/star["FLUX_AUTO"])
                relative_fwhms.append(nearest_new_star["FWHM_IMAGE"]/star["FWHM_IMAGE"])
                relative_flux_radiuses.append(nearest_new_star["FLUX_RADIUS"]/star["FLUX_RADIUS"])

        median_relative_fluxes = np.median(relative_fluxes)
        median_relative_fwhm = np.median(relative_fwhms)
        median_relative_flux_radius = np.median(relative_flux_radiuses)
        score = (median_relative_fluxes + 1/median_relative_flux_radius + 1/median_relative_fwhm) / 3
        self.scores[-1].append(score)

        self.imagPanel.plot_data(self.scores)
        self.root.after(1000, self.cycle)


MainApplication()

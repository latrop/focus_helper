#! /usr/bin/env/python

import os
from os import path
import subprocess
import numpy as np


def call_SE(fitsFile, catName=None, addString=None):
    if os.name == "nt":  # Windows OS
        pathToSex = path.join('lib', 'sex.exe')
    else:
        pathToSex = 'sex'
    pathToFile = path.join("lib", "default.sex")
    callString = " ".join([pathToSex, fitsFile, '-c %s ' % pathToFile, "-VERBOSE_TYPE QUIET"])
    if catName is not None:
        callString += " -CATALOG_NAME %s " % catName
    if addString is not None:
        callString += addString
    subprocess.call(callString, shell=True)


class SExCatalogue(object):
    def __init__(self, catFileName):
        self.filepath = catFileName
        self.objectList = []
        self.legend = []
        self.inds = [-1]
        self.index = 0  # For iterations
        # Parse SE catalogue with arbitrary number of objects and parameters
        for line in open(catFileName):
            sLine = line.strip()
            obj = {}
            if sLine.startswith("#"):
                # legend line
                self.legend.append(sLine.split()[2])
                self.inds.insert(-1, int(sLine.split()[1]) - 1)
                continue
            params = [float(p) for p in line.split()]
            for i in range(len(self.legend)):
                b = self.inds[i]
                e = self.inds[i+1]
                if e == b + 1:
                    obj[self.legend[i]] = params[b]
                else:
                    obj[self.legend[i]] = params[b:e]
            obj[self.legend[-1]] = params[-1]
            self.objectList.append(obj)
        self.numOfObjects = len(self.objectList)

    def find_nearest(self, x, y):
        """ Returns nearest object to given coordinates"""
        nearest = min(self.objectList, key=lambda obj: np.hypot(x-obj["X_IMAGE"], y-obj["Y_IMAGE"]))
        dist = np.hypot(x-nearest["X_IMAGE"], y-nearest["Y_IMAGE"])
        if (dist < 4.0) and (nearest["FLUX_AUTO"] > 0.0):
            return nearest
        else:
            return None

    def get_median_value(self, parameter):
        return np.median([obj[parameter] for obj in self.objectList])

    def get_sigma_value(self, parameter):
        return np.std([obj[parameter] for obj in self.objectList])

    def get_all_values(self, parameter):
        return np.array([obj[parameter] for obj in self.objectList])

    def get_top_n(self, parameter, n):
        """
        Function returns n object with larger parameter value
        """
        return sorted([obj for obj in self.objectList], key=lambda x: x[parameter])[-n:]

    def filter(self, parameter, function):
        """
        Methods filters out all objects fot which function(parameter)
        doesn't return True
        """
        self.objectList = [obj for obj in self.objectList if function(obj[parameter])]
        self.numOfObjects = len(self.objectList)

    def sigma_rejection(self, parameter, number_of_sigmas=2):
        """
        Perform iterative sigma rejection of objects by a given parameter
        """
        while 1:
            if self.numOfObjects <= 5:
                return
            median_value = np.median([obj[parameter] for obj in self.objectList])
            sigma_value = np.std([obj[parameter] for obj in self.objectList])
            lower = median_value - number_of_sigmas * sigma_value
            upper = median_value + number_of_sigmas * sigma_value
            filtered = [obj for obj in self.objectList if lower < obj[parameter] < upper]
            if len(filtered) == self.numOfObjects:
                # No objects were filtered out
                self.numOfObjects = len(filtered)
                return
            self.objectList = filtered
            self.numOfObjects = len(filtered)

    def __iter__(self):
        return self

    def __next__(self):
        """ Iteration method """
        if self.index < self.numOfObjects:
            self.index += 1
            return self.objectList[self.index-1]
        else:
            self.index = 1
            raise StopIteration

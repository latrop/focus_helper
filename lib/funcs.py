#! /usr/bin/env python

from os import path


def get_focus_value(file_path):
    file_name = path.splitext(path.basename(file_path))[0]
    return float(file_name.split("_")[-1][:-2])

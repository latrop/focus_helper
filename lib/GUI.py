#! /usr/bin/env python

import tkinter as Tk
import pylab
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class ImagPanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.mainGraph = pylab.Figure(figsize=(5, 7), dpi=100)
        self.canvas = FigureCanvasTkAgg(self.mainGraph, master=self.window.root)
        self.fig = self.mainGraph.add_subplot(111)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(column=0, row=0)
        self.plot_instances = []

    def clear_plot(self):
        self.fig.clear()

    def plot_data(self, scores_list):
        self.clear_plot()
        mean_values = [np.median(_) for _ in scores_list]
        std_values = [np.std(_) for _ in scores_list]
        self.fig.set_xticks(list(range(len(mean_values))))
        self.fig.set_xticklabels(self.window.focus_values, rotation='vertical')
        self.fig.errorbar(x=list(range(len(mean_values))), y=mean_values,
                          yerr=std_values, fmt="ro")
        print(self.plot_instances)
        self.canvas.draw()
